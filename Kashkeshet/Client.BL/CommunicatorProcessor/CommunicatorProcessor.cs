﻿using Client.BL.MessageProcessors.Abstractions;
using Common.CommunicationMessages.Abstractions;
using Common.CommunicationMessages.Types;
using Common.IO.Communicators.Abstractions;
using log4net;
using System.Collections.Generic;
using System.Reflection;

namespace Client.BL.CommunicatorProcessor
{
    public class CommunicatorProcessor : ICommunicatorProcessor
    {
        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private IDictionary<ServerMessageType, IServerMessageProcessor> _messageTypeToProcessor;
        
        public CommunicatorProcessor(IDictionary<ServerMessageType, IServerMessageProcessor> messageTypeToProcessor)
        {
            _messageTypeToProcessor = messageTypeToProcessor;
        }

        public void ProcessCommunicator(ICommunicator communicator)
        {
            try
            {
                while (true)
                {
                    var serverMessage = (IServerMessage<dynamic>)communicator.Receive();
                    var parser = _messageTypeToProcessor[serverMessage.Type];
                    parser.Process(serverMessage, communicator);
                    log.Info($"Communicator {communicator.GetHashCode()} recieved: {serverMessage}.");
                }
            }
            catch
            {
                log.Info($"Lost connection to server {communicator.GetHashCode()}");
            }
        }
    }
}

﻿using Client.BL.RequestSenders.Abstraction;
using Common.CommunicationMessages.Abstractions;
using Common.IO.Communicators.Abstractions;
using Common.Types.Messages.Abstractions;
using log4net;
using Server.BL.Factories;
using System.Reflection;

namespace Client.BL.RequestSenders
{
    public class SignupRequestSender : IClientRequestSender
    {
        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public void SendRequest(dynamic request, ICommunicator communicator)
        {
            var requestFactory = new SignupRequestFactory(request);
            communicator.Send(requestFactory.CreateDTO());
            log.Info($"Sent SignupRequest with username: {request}");
        }
    }
}

﻿using Common.CommunicationMessages.Abstractions;
using Common.IO.Communicators.Abstractions;

namespace Client.BL.RequestSenders.Abstraction
{
    public interface IClientRequestSender
    {
        void SendRequest(dynamic request, ICommunicator communicator);
    }
}

﻿using Client.BL.RequestSenders.Abstraction;
using Common.CommunicationMessages.Abstractions;
using Common.IO.Communicators.Abstractions;
using Common.Types.Messages.Abstractions;
using log4net;
using Server.BL.Factories;
using System.Reflection;

namespace Client.BL.RequestSenders
{
    public class SendMessageRequestSender : IClientRequestSender
    {
        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public void SendRequest(dynamic request, ICommunicator communicator)
        {
            var requestFactory = new SendMessageRequestFactory(request);
            communicator.Send(requestFactory.CreateDTO());
            log.Info($"Sent SendMessageRequest: {request}");
        }
    }
}

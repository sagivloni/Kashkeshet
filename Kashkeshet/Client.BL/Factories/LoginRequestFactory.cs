﻿using Common.CommunicationMessages.ClientMessages;
using Common.ObjectCreators;

namespace Server.BL.Factories
{
    public class LoginRequestFactory : IDTOFactory
    {
        private string _username;

        public LoginRequestFactory(string username) => _username = username;

        public object CreateDTO() => new LoginRequest(_username);
    }
}

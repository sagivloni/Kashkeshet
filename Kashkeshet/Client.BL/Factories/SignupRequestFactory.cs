﻿using Common.CommunicationMessages.ClientMessages;
using Common.ObjectCreators;

namespace Server.BL.Factories
{
    public class SignupRequestFactory : IDTOFactory
    {
        private string _username;

        public SignupRequestFactory(string username) => _username = username;

        public object CreateDTO() => new SignupRequest(_username);
    }
}

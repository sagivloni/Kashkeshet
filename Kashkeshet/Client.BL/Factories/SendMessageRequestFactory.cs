﻿using Common.CommunicationMessages.ClientMessages;
using Common.ObjectCreators;
using Common.Types.Messages.Abstractions;

namespace Server.BL.Factories
{
    public class SendMessageRequestFactory : IDTOFactory
    {
        private IMessage _message;

        public SendMessageRequestFactory(IMessage message) => _message = message;

        public object CreateDTO() => new SendMessageRequest(_message);
    }
}

﻿using Client.BL.MessageProcessors.Abstractions;
using Common.EventHandlers;
using Common.CommunicationMessages.Abstractions;
using Common.IO.Communicators.Abstractions;
using log4net;
using System.Reflection;

namespace Server.BL.MessageProcessors.Response
{
    public class LoginResponseProcessor : IServerMessageProcessor
    {
        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IServerEventHandler _serverEventHandler;

        public LoginResponseProcessor(IServerEventHandler serverEventHandler) => _serverEventHandler = serverEventHandler;

        public void Process(IServerMessage<dynamic> message, ICommunicator communicator)
        {
            var response = (ServerResponseBase<dynamic>)message;
            log.Info($"Login with username: {response.Data}. Succeeded: {response.Status}");
            _serverEventHandler.OnLogin(response.Status);
        }
    }
}

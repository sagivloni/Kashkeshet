﻿using Client.BL.MessageProcessors.Abstractions;
using Common.EventHandlers;
using Common.CommunicationMessages.Abstractions;
using Common.IO.Communicators.Abstractions;
using log4net;
using System.Reflection;

namespace Server.BL.MessageProcessors
{
    public class SignupResponseProcessor : IServerMessageProcessor
    {
        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IServerEventHandler _serverEventHandler;

        public SignupResponseProcessor(IServerEventHandler serverEventHandler) => _serverEventHandler = serverEventHandler;

        public void Process(IServerMessage<dynamic> message, ICommunicator communicator)
        {
            var response = (ServerResponseBase<dynamic>)message;
            log.Info($"Signup with username: {response.Data}. Succeeded: {response.Status}");
            _serverEventHandler.OnSignup(response.Status);
        }
    }
}

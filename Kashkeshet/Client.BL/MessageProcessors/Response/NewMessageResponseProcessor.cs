﻿using Client.BL.MessageProcessors.Abstractions;
using Common.EventHandlers;
using Common.CommunicationMessages.Abstractions;
using Common.IO.Communicators.Abstractions;
using Common.Types.Messages.Abstractions;
using log4net;
using System.Reflection;

namespace Server.BL.MessageProcessors.Response
{
    public class SendMessageResponseProcessor : IServerMessageProcessor
    {
        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IServerEventHandler _serverEventHandler;

        public SendMessageResponseProcessor(IServerEventHandler serverEventHandler) => _serverEventHandler = serverEventHandler;

        public void Process(IServerMessage<dynamic> message, ICommunicator communicator)
        {
            var response = (ServerResponseBase<dynamic>)message;
            var msg = (IMessage)response.Data;
            log.Info($"Sent message {msg.Content} to {msg.Destination}.");
            _serverEventHandler.OnSignup(response.Status);
        }
    }
}

﻿using Client.BL.MessageProcessors.Abstractions;
using Common.EventHandlers;
using Common.CommunicationMessages.Abstractions;
using Common.DB.Abstractions;
using Common.IO.Communicators.Abstractions;
using log4net;
using System.Reflection;

namespace Server.BL.MessageProcessors.Response
{
    public class NewMessageUpdateProcessor : IServerMessageProcessor
    {
        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IServerEventHandler _serverEventHandler;
        private readonly IChatDB _chatDB;

        public NewMessageUpdateProcessor(IServerEventHandler serverEventHandler, IChatDB chatDB)
        {
            _serverEventHandler = serverEventHandler;
            _chatDB = chatDB;
        }

        public void Process(IServerMessage<dynamic> message, ICommunicator communicator)
        {
            var msg = message.Data;
            log.Info($"Recieved message: {msg}.");
            _serverEventHandler.OnNewMessage(message.Data);
        }
    }
}

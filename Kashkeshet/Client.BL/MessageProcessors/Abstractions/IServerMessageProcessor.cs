﻿using Common.CommunicationMessages.Abstractions;
using Common.IO.Communicators.Abstractions;

namespace Client.BL.MessageProcessors.Abstractions
{
    public interface IServerMessageProcessor
    {
        void Process(IServerMessage<dynamic> message, ICommunicator communicator);
    }
}

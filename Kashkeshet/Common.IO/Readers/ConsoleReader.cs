﻿using Common.IO.Readers.Abstractions;
using System;

namespace Common.IO
{
    public class ConsoleReader : IReader
    {
        public string Read()
        {
            return Console.ReadLine();
        }
    }
}

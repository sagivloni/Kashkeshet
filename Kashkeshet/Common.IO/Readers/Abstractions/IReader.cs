﻿namespace Common.IO.Readers.Abstractions
{
    public interface IReader
    {
        string Read();
    }
}

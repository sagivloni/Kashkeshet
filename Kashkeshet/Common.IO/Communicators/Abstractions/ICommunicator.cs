﻿namespace Common.IO.Communicators.Abstractions
{
    public interface ICommunicator
    {
        void Send(object obj);
        object Receive();
    }
}

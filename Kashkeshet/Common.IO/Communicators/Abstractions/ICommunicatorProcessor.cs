﻿namespace Common.IO.Communicators.Abstractions
{
    public interface ICommunicatorProcessor
    {
        void ProcessCommunicator(ICommunicator communicator);
    }
}

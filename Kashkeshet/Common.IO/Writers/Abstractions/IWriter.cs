﻿
namespace Common.IO.Writers.Abstractions
{
    public interface IWriter
    {
        void Write(string output);
    }
}

﻿using Common.IO.Writers.Abstractions;
using System;

namespace Common.IO.Writers
{
    public class ClearConsoleWriter : IWriter
    {
        public void Write(string output)
        {
            Console.Clear();
            Console.WriteLine(output);
        }
    }
}

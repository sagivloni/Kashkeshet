﻿using Common.IO.Writers.Abstractions;
using System;

namespace Common.IO.Writers
{
    public class ConsoleWriter : IWriter
    {
        public void Write(string output)
        {
            Console.WriteLine(output);
        }
    }
}

﻿using Common.IO.Readers.Abstractions;
using Common.IO.Recievers.Abstractions;
using Common.IO.Writers.Abstractions;
using System;
using System.Net;

namespace Common.IO.Recievers
{
    public class SocketParamaterReciever : IReciever<Tuple<IPAddress, int>>
    {
        private IReader _reader;
        private IWriter _writer;

        public SocketParamaterReciever(IReader reader, IWriter writer)
        {
            _reader = reader;
            _writer = writer;
        }

        public Tuple<IPAddress, int> Recieve()
        {
            return new Tuple<IPAddress, int>(GetSocketIPAddress(), GetSocketPort());
        }

        private IPAddress GetSocketIPAddress()
        {
            bool validIp;
            IPAddress ip;
            do
            {
                _writer.Write("Enter the server IP address");
                string ipAddress = _reader.Read();
                validIp = IPAddress.TryParse(ipAddress, out ip);
            } while (!validIp);
            return ip;
        }

        private int GetSocketPort()
        {
            bool validPort;
            int port;
            do
            {
                _writer.Write("Enter the server port");
                validPort = int.TryParse(_reader.Read(), out port);
                validPort = validPort && port > 0 && port < 65536;
            } while (!validPort);
            return port;
        }
    }
}

﻿namespace Common.ObjectCreators
{
    public interface IDTOFactory
    {
        object CreateDTO();
    }
}

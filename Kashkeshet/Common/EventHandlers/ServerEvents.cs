﻿using Common.Types.Chats.Abstractions;
using Common.Types.Messages.Abstractions;
using System;

namespace Common.EventHandlers
{
    public class ServerEvents : IServerEventHandler
    {
        public event Action<IMessage> NewMessage;
        public event Action<bool> Login;
        public event Action<bool> Signup;
        public event Action<IChat> NewChat;
        public event Action<IChat> NewGroup;

        public void OnLogin(bool isSucceeded) => Login?.Invoke(isSucceeded);
        public void OnNewMessage(IMessage message) => NewMessage?.Invoke(message);
        public void OnSignup(bool isSucceded) => Signup?.Invoke(isSucceded);
        public void OnNewChat(IChat chat) => NewChat?.Invoke(chat);
        public void OnNewGroup(IChat group) => NewGroup?.Invoke(group);
    }
}

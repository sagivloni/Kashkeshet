﻿using Common.Types.Chats.Abstractions;
using Common.Types.Messages.Abstractions;

namespace Common.EventHandlers
{
    public interface IServerEventHandler
    {
        void OnNewMessage(IMessage message);
        void OnLogin(bool isSucceeded);
        void OnSignup(bool isSucceeded);
        void OnNewChat(IChat chat);
        void OnNewGroup(IChat group);
    }
}

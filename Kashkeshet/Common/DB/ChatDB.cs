﻿using System;
using System.Collections.Generic;
using Common.DB.Abstractions;
using Common.Types.Chats.Abstractions;
using Common.EventHandlers;
using Common.Types.Messages.Abstractions;

namespace Common.DB
{
    public class ChatDB : IChatDB
    {
        public IDictionary<Guid, IChat> Chats { get; }
        private readonly ServerEvents _serverEvents;

        public ChatDB(ServerEvents serverEvents, IDictionary<Guid, IChat> Chats = null)
        {
            Chats = Chats ?? new Dictionary<Guid, IChat>();
            _serverEvents = serverEvents;
            _serverEvents.NewMessage += SendMessage;
            _serverEvents.NewChat += AddChat;
            _serverEvents.NewGroup += AddChat;
        }

        public void AddChat(IChat chat) => Chats[chat.Chat.Id] = chat;
        public IChat GetChatById(Guid id) => Chats[id];
        public void SendMessage(IMessage message) => Chats[message.Destination].SendMessage(message);
    }

}

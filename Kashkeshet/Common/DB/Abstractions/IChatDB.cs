﻿using Common.Types.Chats.Abstractions;
using Common.Types.Messages.Abstractions;
using System;
using System.Collections.Generic;

namespace Common.DB.Abstractions
{
    public interface IChatDB
    {
        IDictionary<Guid, IChat> Chats { get; }
        IChat GetChatById(Guid id);
        void AddChat(IChat chat);
        void SendMessage(IMessage message);
    }
}

﻿using Common.Types.DTOs;
using System;

namespace Common.Types.Messages.Abstractions
{
    public interface IMessage
    {
        UserInfo Author { get; }
        DateTime CreationTime { get; }
        Guid Destination { get; }
        dynamic Content { get; }
    }
}

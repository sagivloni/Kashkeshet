﻿using System;
using Common.Types.Messages.Abstractions;
using Common.Types.DTOs;

namespace Common.Types.Messages
{
    public class BasicMessage : IMessage
    {
        public UserInfo Author { get; }
        public DateTime CreationTime { get; }
        public Guid Destination { get; }
        public dynamic Content { get; }

        public BasicMessage(UserInfo author, DateTime creationTime, string content, Guid destination)
        {
            Author = author;
            CreationTime = creationTime;
            Content = content;
            Destination = destination;
        }
    }
}

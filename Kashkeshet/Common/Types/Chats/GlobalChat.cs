﻿using Common.Types.Chats.Abstractions;
using Common.Types.DTOs;
using System;

namespace Common.Types.Chats
{
    public class GlobalChat : ChatBase
    {
        public GlobalChat(ChatInfo chat) : base(chat)
        {
        }
    }
}

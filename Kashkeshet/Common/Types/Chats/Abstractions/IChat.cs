﻿using Common.Types.DTOs;
using Common.Types.Messages.Abstractions;

namespace Common.Types.Chats.Abstractions
{
    public interface IChat
    {
        ChatInfo Chat { get; }
        void SendMessage(IMessage message);
        void AddParticipator(UserInfo user);
        void RemoveParticipator(UserInfo user);
    }
}

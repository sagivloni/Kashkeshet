﻿using Common.Types.DTOs;
using Common.Types.Messages.Abstractions;

namespace Common.Types.Chats.Abstractions
{
    public class ChatBase : IChat
    {
        public ChatInfo Chat { get; }

        public ChatBase(ChatInfo chat) => Chat = chat;

        public void AddParticipator(UserInfo user) => Chat.Members.Add(user);

        public void RemoveParticipator(UserInfo user) => Chat.Members.Remove(user);

        public void SendMessage(IMessage message) => Chat.History.Add(message);
    }
}

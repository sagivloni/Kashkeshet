﻿namespace Common.Types.DTOs
{
    public enum UserStatus
    {
        Connected, Disconnected
    }
}

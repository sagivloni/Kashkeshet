﻿using Common.Types.Messages.Abstractions;
using System;
using System.Collections.Generic;

namespace Common.Types.DTOs
{
    public class ChatInfo
    {
        public string Name { get; }
        public Guid Id { get; }
        public IList<UserInfo> Members { get; set; }
        public IList<IMessage> History { get; set; }

        public ChatInfo(string name, Guid id, IList<UserInfo> members = null, IList<IMessage> history = null)
        {
            Name = name;
            Id = id;
            Members = members ?? new List<UserInfo>();
            History = history ?? new List<IMessage>();
        }
    }
}

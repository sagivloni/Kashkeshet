﻿using System;

namespace Common.Types.DTOs
{
    public class UserInfo
    {
        public string Name { get; }
        public Guid Id { get; }
        public UserStatus Status { get; set; }

        public UserInfo(string name, Guid id, UserStatus status)
        {
            Name = name;
            Id = id;
            Status = status;
        }
    }
}

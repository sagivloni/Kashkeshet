﻿namespace Common.CommunicationMessages.Types
{
    public enum ServerMessageType
    {
        LoginResponse,
        SignupResponse,
        SendMessageResponse,
        CreateGroupResponse,
        CreateChatResponse,
        NewChatUpdate,
        NewMessageUpdate,
        NewGroupUpdate
    }
}

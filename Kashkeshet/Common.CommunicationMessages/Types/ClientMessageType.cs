﻿namespace Common.CommunicationMessages.Types
{
    public enum ClientMessageType
    {
        LoginRequest,
        SignupRequest,
        SendMessageRequest,
        CreateGroupRequest,
        CreateChatRequest
    }
}

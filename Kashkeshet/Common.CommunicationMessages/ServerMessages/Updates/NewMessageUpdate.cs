﻿using Common.CommunicationMessages.Abstractions;
using Common.CommunicationMessages.Types;
using Common.Types.Messages.Abstractions;

namespace Common.CommunicationMessages.ServerMessages.Updates
{
    public class NewMessageUpdate : ServerUpdateBase<IMessage>
    {
        public NewMessageUpdate(IMessage chat, ServerMessageType type = ServerMessageType.NewMessageUpdate)
            : base(type, chat)
        {
        }
    }
}

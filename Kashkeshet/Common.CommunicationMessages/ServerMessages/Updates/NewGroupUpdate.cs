﻿using Common.CommunicationMessages.Abstractions;
using Common.CommunicationMessages.Types;
using Common.Types.Chats.Abstractions;

namespace Common.CommunicationMessages.ServerMessages.Updates
{
    public class NewGroupUpdate : ServerUpdateBase<IChat>
    {
        public NewGroupUpdate(IChat chat, ServerMessageType type = ServerMessageType.NewGroupUpdate)
            : base(type, chat)
        {
        }
    }
}

﻿using Common.CommunicationMessages.Abstractions;
using Common.CommunicationMessages.Types;
using Common.Types.Chats.Abstractions;

namespace Common.CommunicationMessages.ServerMessages.Updates
{
    public class NewChatUpdate : ServerUpdateBase<IChat>
    {
        public NewChatUpdate(IChat chat, ServerMessageType type = ServerMessageType.NewChatUpdate)
            : base(type, chat)
        {
        }
    }
}

﻿using Common.CommunicationMessages.Abstractions;
using Common.CommunicationMessages.Types;
using Common.Types.Messages.Abstractions;

namespace Common.CommunicationMessages.ServerMessages.Responses
{
    public class SendMessageResponse : ServerResponseBase<IMessage>
    {
        public SendMessageResponse(IMessage message,
            bool isSent,
            ServerMessageType type = ServerMessageType.SendMessageResponse)
            : base(type, message, isSent)
        {
        }
    }
}
﻿using Common.CommunicationMessages.Abstractions;
using Common.CommunicationMessages.Types;

namespace Common.CommunicationMessages.ServerMessages.Responses
{
    public class SignupResponse : ServerResponseBase<string>
    {
        public SignupResponse(string username,
            bool isApproved,
            ServerMessageType type = ServerMessageType.CreateGroupResponse)
            : base(type, username, isApproved)
        {
        }
    }
}
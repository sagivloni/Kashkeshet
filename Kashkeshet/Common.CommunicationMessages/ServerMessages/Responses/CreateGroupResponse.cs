﻿using Common.CommunicationMessages.Abstractions;
using Common.CommunicationMessages.Types;
using Common.Types.Chats.Abstractions;

namespace Common.CommunicationMessages.ServerMessages.Responses
{
    public class CreateGroupResponse : ServerResponseBase<IChat>
    {
        public CreateGroupResponse(IChat message, 
            bool isSent,
            ServerMessageType type = ServerMessageType.CreateGroupResponse)
            : base(type, message, isSent)
        {
        }
    }
}

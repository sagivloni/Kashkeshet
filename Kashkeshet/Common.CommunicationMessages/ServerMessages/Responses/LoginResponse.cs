﻿using Common.CommunicationMessages.Abstractions;
using Common.CommunicationMessages.Types;

namespace Common.CommunicationMessages.ServerMessages.Responses
{
    public class LoginResponse : ServerResponseBase<string>
    {
        public LoginResponse(string username,
            bool isApproved,
            ServerMessageType type = ServerMessageType.LoginResponse)
            : base(type, username, isApproved)
        {
        }
    }
}

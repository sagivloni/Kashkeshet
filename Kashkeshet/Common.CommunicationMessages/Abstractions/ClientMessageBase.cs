﻿using Common.CommunicationMessages.Types;

namespace Common.CommunicationMessages.Abstractions
{
    public abstract class ClientMessageBase<T>
    {
        public ClientMessageType Type { get; }
        public T Data { get; protected set; }

        public ClientMessageBase(ClientMessageType type, T data)
        {
            Type = type;
            Data = data;
        }
    }
}

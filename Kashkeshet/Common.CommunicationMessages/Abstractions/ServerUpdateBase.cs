﻿using Common.CommunicationMessages.Types;
using System;

namespace Common.CommunicationMessages.Abstractions
{
    public abstract class ServerUpdateBase<T> : IServerMessage<T>
    {
        public ServerMessageType Type { get; }
        public T Data { get; protected set; }
        
        public ServerUpdateBase(ServerMessageType type, T data)
        {
            Type = type;
            Data = data;
        }
    }
}

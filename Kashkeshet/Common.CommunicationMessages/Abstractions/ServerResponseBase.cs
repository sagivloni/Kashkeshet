﻿using Common.CommunicationMessages.Types;

namespace Common.CommunicationMessages.Abstractions
{
    public abstract class ServerResponseBase<T> : ServerUpdateBase<T>
    {
        public bool Status { get; protected set; }

        public ServerResponseBase(ServerMessageType type, T data, bool status) : base(type, data)
        {
            Status = status;
        }
    }
}

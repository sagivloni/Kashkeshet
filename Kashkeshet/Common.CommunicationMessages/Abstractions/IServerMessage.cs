﻿using Common.CommunicationMessages.Types;

namespace Common.CommunicationMessages.Abstractions
{
    public interface IServerMessage<T>
    {
        T Data { get; }
        ServerMessageType Type { get; }
    }
}
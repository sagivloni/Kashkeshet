﻿using Common.CommunicationMessages.Abstractions;
using Common.CommunicationMessages.Types;
using Common.Types.Chats.Abstractions;

namespace Common.CommunicationMessages.ClientMessages
{
    public class CreateChatRequest : ClientMessageBase<IChat>
    {
        public CreateChatRequest(IChat chat, ClientMessageType type = ClientMessageType.CreateChatRequest) :
            base(type, chat)
        {
        }
    }
}

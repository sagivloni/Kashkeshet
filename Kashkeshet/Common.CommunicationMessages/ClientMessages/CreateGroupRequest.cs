﻿using Common.CommunicationMessages.Abstractions;
using Common.CommunicationMessages.Types;
using Common.Types.Chats.Abstractions;

namespace Common.CommunicationMessages.ClientMessages
{
    public class CreateGroupRequest : ClientMessageBase<IChat>
    {
        public CreateGroupRequest(IChat chat, ClientMessageType type = ClientMessageType.CreateGroupRequest) :
            base(type, chat)
        {           
        }
    }
}

﻿using Common.CommunicationMessages.Abstractions;
using Common.CommunicationMessages.Types;
using Common.Types.Messages.Abstractions;

namespace Common.CommunicationMessages.ClientMessages
{
    public class SendMessageRequest : ClientMessageBase<IMessage>
    {
        public SendMessageRequest(IMessage message, ClientMessageType type = ClientMessageType.SendMessageRequest)
            : base(type, message)
        {
        }
    }
}

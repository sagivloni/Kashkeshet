﻿using Common.CommunicationMessages.Abstractions;
using Common.CommunicationMessages.Types;

namespace Common.CommunicationMessages.ClientMessages
{
    public class LoginRequest : ClientMessageBase<string>
    {
        public LoginRequest(string username, ClientMessageType type = ClientMessageType.LoginRequest) :
            base(type, username)
        {
        }
    }
}

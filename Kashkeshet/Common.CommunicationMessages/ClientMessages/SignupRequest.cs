﻿using Common.CommunicationMessages.Abstractions;
using Common.CommunicationMessages.Types;

namespace Common.CommunicationMessages.ClientMessages
{
    public class SignupRequest : ClientMessageBase<string>
    {
        public SignupRequest(string username, ClientMessageType type = ClientMessageType.SignupRequest) : base(type, username)
        {
        }
    }
}

﻿using Server.BL.DBBridge;
using System;
using System.Collections.Generic;
using Common.Types.DTOs;
using System.Linq;

namespace Server.DB
{
    public class UserDB : IUserDB
    {
        public IDictionary<Guid, UserInfo> Users { get; }

        public void AddUser(UserInfo user) => Users[user.Id] = user;

        public void AddUser(string name) => AddUser(new UserInfo(name, Guid.NewGuid(), UserStatus.Connected));

        public IList<string> GetNames() => Users.Values.Select(user => user.Name).ToList();

        public UserInfo GetUserById(Guid id) => Users[id];
    }
}

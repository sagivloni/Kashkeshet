﻿using Common.CommunicationMessages.Types;
using Common.IO;
using Common.IO.Readers.Abstractions;
using Common.IO.Recievers;
using Common.IO.Writers;
using Common.IO.Writers.Abstractions;
using Common.DB;
using Common.EventHandlers;
using Server.BL.CommunicatorProcessor;
using Server.BL.MessageProcessors;
using Server.BL.MessageProcessors.Abstractions;
using Server.DB;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;

namespace Server.App
{
    public class Bootstrapper
    {
        public ServerToProcessor Init()
        {
            // Server
            IWriter writer = new ConsoleWriter();
            IReader reader = new ConsoleReader();
            var socketParamaterReciever = new SocketParamaterReciever(reader, writer);
            var ipToPort = socketParamaterReciever.Recieve();
            var listener = new TcpListener(ipToPort.Item1, ipToPort.Item2);
            listener.Start();
            var formatter = new BinaryFormatter();
            var server = new Server(listener, formatter);

            ServerEvents serverEvents = new ServerEvents();
            // Databases
            var chatDB = new ChatDB(serverEvents);
            var userDB = new UserDB();

            //CommunicatorProcessor
            var messageTypeToProcessor = new Dictionary<ClientMessageType, IClientMessageProcessor>
            {
                { ClientMessageType.SendMessageRequest, new SendMessageRequestProcessor(chatDB)},
                { ClientMessageType.LoginRequest, new LoginRequestProcessor(userDB)},
                { ClientMessageType.SignupRequest, new SignupRequestProcessor(userDB)}
            };
            var communicatorProcessor = new CommunicatorProcessor(messageTypeToProcessor);

            return new ServerToProcessor(server, communicatorProcessor);
        }
    }
}
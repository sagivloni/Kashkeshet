﻿using Common.IO.Communicators;
using Common.IO.Communicators.Abstractions;
using log4net;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Server.App
{
    public class Server
    {
        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly TcpListener _listener;

        private IFormatter _formatter;

        public Server(TcpListener listener, IFormatter formatter)
        {
            _listener = listener;
            _formatter = formatter;
        }

        public void Serve(ICommunicatorProcessor communicatorProcessor)
        {
            ListenForTcpClient(communicatorProcessor);
        }

        private void ListenForTcpClient(ICommunicatorProcessor communicatorProcessor)
        {
            while (true)
            {
                var client = _listener.AcceptTcpClient();
                log.Info($"Accepted client: {client.GetHashCode()}"); 
                ICommunicator communicator = new TcpCommunicator(client, _formatter);
                Task.Run(() =>
                {
                    communicatorProcessor.ProcessCommunicator(communicator);
                });
            }
        }
    }
}
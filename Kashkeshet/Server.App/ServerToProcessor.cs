﻿using Common.IO.Communicators.Abstractions;

namespace Server.App
{
    public class ServerToProcessor
    {
        public ICommunicatorProcessor Processor { get; }
        public Server Server { get; }

        public ServerToProcessor(Server server, ICommunicatorProcessor processor)
        {
            Processor = processor;
            Server = server;
        }
    }
}

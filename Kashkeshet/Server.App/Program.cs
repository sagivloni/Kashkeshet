﻿namespace Server.App
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var bootstrapper = new Bootstrapper();
            var serverToProcessor = bootstrapper.Init();
            serverToProcessor.Server.Serve(serverToProcessor.Processor);
        }
    }
}

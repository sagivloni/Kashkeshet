﻿using Common.Types.DTOs;
using System;
using System.Collections.Generic;

namespace Server.BL.DBBridge
{
    public interface IUserDB
    {
        IDictionary<Guid, UserInfo> Users { get; }
        void AddUser(UserInfo user);
        void AddUser(string name);
        UserInfo GetUserById(Guid id);
        IList<string> GetNames();
    }
}

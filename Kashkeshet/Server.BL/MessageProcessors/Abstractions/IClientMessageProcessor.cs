﻿using Common.CommunicationMessages.Abstractions;
using Common.IO.Communicators.Abstractions;

namespace Server.BL.MessageProcessors.Abstractions
{
    public interface IClientMessageProcessor
    {
        void Process(ClientMessageBase<dynamic> message, ICommunicator communicator);
    }
}

﻿using Common.CommunicationMessages.Abstractions;
using Common.DB.Abstractions;
using Common.IO.Communicators.Abstractions;
using log4net;
using Server.BL.DBBridge;
using Server.BL.Factories;
using Server.BL.MessageProcessors.Abstractions;
using System.Reflection;

namespace Server.BL.MessageProcessors
{
    public class LoginRequestProcessor : IClientMessageProcessor
    {
        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IUserDB _userDB;

        public LoginRequestProcessor(IUserDB userDB) => _userDB = userDB;

        public void Process(ClientMessageBase<dynamic> message, ICommunicator communicator)
        {
            var username = message.Data;
            var canLogIn = _userDB.GetNames().Contains(username);
            var responseFactory = new SignupResponseFactory(username, canLogIn);
            log.Info($"Signup with username: {username}. Succeeded: {canLogIn}");
            communicator.Send(responseFactory.CreateDTO());
        }
    }
}

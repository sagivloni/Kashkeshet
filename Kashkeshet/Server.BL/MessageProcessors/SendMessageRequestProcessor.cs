﻿using Common.CommunicationMessages.Abstractions;
using Common.DB.Abstractions;
using Common.IO.Communicators.Abstractions;
using Common.Types.Messages.Abstractions;
using log4net;
using Server.BL.DBBridge;
using Server.BL.Factories;
using Server.BL.MessageProcessors.Abstractions;
using System.Reflection;

namespace Server.BL.MessageProcessors
{
    public class SendMessageRequestProcessor : IClientMessageProcessor
    {
        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IChatDB _chatDB;

        public SendMessageRequestProcessor(IChatDB chatDB) => _chatDB = chatDB;

        public void Process(ClientMessageBase<dynamic> message, ICommunicator communicator)
        {
            var msg = message.Data;
            _chatDB.Chats[msg.Destination].SendMessage(msg);
            var responseFactory = new SendMessageResponseFactory(msg, true);
            log.Info($"{msg.Author} Sent message to {msg.Destination}.");
            communicator.Send(responseFactory.CreateDTO());

        }
    }
}

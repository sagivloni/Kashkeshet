﻿using Common.CommunicationMessages.ServerMessages.Responses;
using Common.ObjectCreators;

namespace Server.BL.Factories
{
    public class LoginResponseFactory : IDTOFactory
    {
        private string _username;
        private bool _isLoggedIn;

        public LoginResponseFactory(string username, bool isLoggedIn)
        {
            _username = username;
            _isLoggedIn = isLoggedIn;
        }

        public object CreateDTO() => new LoginResponse(_username, _isLoggedIn);
    }
}

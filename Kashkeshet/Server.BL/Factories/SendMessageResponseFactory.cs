﻿using Common.CommunicationMessages.ServerMessages.Responses;
using Common.ObjectCreators;
using Common.Types.Messages.Abstractions;

namespace Server.BL.Factories
{
    public class SendMessageResponseFactory : IDTOFactory
    {
        private IMessage _message;
        private bool _isSent;

        public SendMessageResponseFactory(IMessage message, bool isSent)
        {
            _message = message;
            _isSent = isSent;
        }

        public object CreateDTO() => new SendMessageResponse(_message, _isSent);
    }
}

﻿using Common.CommunicationMessages.ServerMessages.Responses;
using Common.ObjectCreators;

namespace Server.BL.Factories
{
    public class SignupResponseFactory : IDTOFactory
    {
        private string _username;
        private bool _isSignedUp;

        public SignupResponseFactory(string username, bool isSignedUp)
        {
            _username = username;
            _isSignedUp = isSignedUp;
        }

        public object CreateDTO() => new SignupResponse(_username, _isSignedUp);
    }
}

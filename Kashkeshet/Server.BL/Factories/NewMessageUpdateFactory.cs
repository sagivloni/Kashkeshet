﻿using Common.CommunicationMessages.ServerMessages.Updates;
using Common.ObjectCreators;
using Common.Types.Messages.Abstractions;

namespace Server.BL.Factories
{
    public class NewMessageUpdateFactory : IDTOFactory
    {
        private IMessage _message;

        public NewMessageUpdateFactory(IMessage message) => _message = message;
        
        public object CreateDTO() => new NewMessageUpdate(_message);
    }
}
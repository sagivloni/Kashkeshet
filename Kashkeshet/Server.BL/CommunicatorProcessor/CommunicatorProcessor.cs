﻿using Common.CommunicationMessages.Abstractions;
using Common.CommunicationMessages.Types;
using Common.IO.Communicators.Abstractions;
using log4net;
using Server.BL.MessageProcessors.Abstractions;
using System.Collections.Generic;
using System.Reflection;

namespace Server.BL.CommunicatorProcessor
{
    public class CommunicatorProcessor : ICommunicatorProcessor
    {
        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private IDictionary<ClientMessageType, IClientMessageProcessor> _messageTypeToProcessor;

        public CommunicatorProcessor(IDictionary<ClientMessageType, IClientMessageProcessor> messageTypeToProcessor)
        {
            _messageTypeToProcessor = messageTypeToProcessor;
        }

        public void ProcessCommunicator(ICommunicator communicator)
        {
            try
            {
                while (true)
                {
                    var clientMessage = (ClientMessageBase<dynamic>)communicator.Receive();
                    var parser = _messageTypeToProcessor[clientMessage.Type];
                    parser.Process(clientMessage, communicator);
                    log.Info($"Communicator {communicator.GetHashCode()} recieved: {clientMessage}.");
                }
            }
            catch
            {
                log.Info($"Client was disconnected {communicator.GetHashCode()}");
            }
        }
    }
}

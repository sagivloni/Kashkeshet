﻿namespace Client.App
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var bootstrapper = new Bootstrapper();
            var client = bootstrapper.Init();
            client.RunClientLoop();
        }
    }
}

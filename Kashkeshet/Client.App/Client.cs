﻿using Client.UI.DisplayRunners.Abstractions;
using Common.IO.Communicators.Abstractions;
using System.Threading.Tasks;

namespace Client.App
{
    public class Client
    {
        private IDisplayRunner _chatDisplayRunner;
        private IDisplayRunner _startupDisplayRunner;
        private ICommunicatorProcessor _communicatorProcessor;
        private ICommunicator _communicator;

        public Client(IDisplayRunner startupDisplayRunner,
            IDisplayRunner chatDisplayRunner,
            ICommunicatorProcessor communicatorProcessor,
            ICommunicator communicator)
        {
            _startupDisplayRunner = startupDisplayRunner;
            _chatDisplayRunner = chatDisplayRunner;
            _communicatorProcessor = communicatorProcessor;
            _communicator = communicator; 
        }

        public void RunClientLoop()
        {
            Task.Run(() => _communicatorProcessor.ProcessCommunicator(_communicator));
            _startupDisplayRunner.Run();
            _chatDisplayRunner.Run();
        }
    }
}
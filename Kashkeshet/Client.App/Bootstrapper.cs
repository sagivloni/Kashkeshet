﻿using Client.BL.CommunicatorProcessor;
using Client.BL.MessageProcessors.Abstractions;
using Client.BL.RequestSenders;
using Client.UI.DisplayRunners;
using Client.UI.Interactors;
using Client.UI.Interactors.Abstractions;
using Client.UI.Recievers;
using Client.UI.Screens;
using Client.UI.Screens.Abstractions;
using Client.UI.Types;
using Common.CommunicationMessages.Types;
using Common.DB;
using Common.IO;
using Common.IO.Communicators;
using Common.IO.Readers.Abstractions;
using Common.IO.Recievers;
using Common.IO.Recievers.Abstractions;
using Common.IO.Writers;
using Common.IO.Writers.Abstractions;
using Common.EventHandlers;
using Common.Types.DTOs;
using Server.BL.MessageProcessors;
using Server.BL.MessageProcessors.Response;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;

namespace Client.App
{
    public class Bootstrapper
    {
        public Client Init()
        {
            IWriter writer = new ConsoleWriter();
            IWriter clearWriter = new ClearConsoleWriter();
            IReader reader = new ConsoleReader();

            var socketParamaterReciever = new SocketParamaterReciever(reader, writer);
            var ipToPort = socketParamaterReciever.Recieve();

            TcpClient client = new TcpClient();
            client.Connect(ipToPort.Item1, ipToPort.Item2);
            var formatter = new BinaryFormatter();
            var communicator = new TcpCommunicator(client, formatter);

            ServerEvents serverEvents = new ServerEvents();
            // Databases
            var chatDB = new ChatDB(serverEvents);

            //CommunicatorProcessor
            var messageTypeToProcessor = new Dictionary<ServerMessageType, IServerMessageProcessor>
            {
                { ServerMessageType.NewMessageUpdate, new NewMessageUpdateProcessor(serverEvents, chatDB)},
                { ServerMessageType.SendMessageResponse, new SendMessageResponseProcessor(serverEvents)},
                { ServerMessageType.LoginResponse, new LoginResponseProcessor(serverEvents)},
                { ServerMessageType.SignupResponse, new SignupResponseProcessor(serverEvents)}
            };
            var communicatorProcessor = new CommunicatorProcessor(messageTypeToProcessor);

            //Chats
            ChatInfo globalChat = new ChatInfo("Global chat", Guid.NewGuid());

            //Screens
            var welcomeScreen = new WelcomeScreen(writer);
            var globalChatScreen = new ChatScreen(writer, globalChat);
            var loginScreen = new LoginScreen(clearWriter);
            var signupScreen = new SignupScreen(clearWriter);

            //Recievers
            var globalChatMessageReciever = new MessageReciever(reader, writer);
            var userActionReciever = new UserActionReciever(reader, writer);
            var usernameReciever = new UsernameReciever(reader, writer);

            //RequestSenders
            var loginRequestSender = new LoginRequestSender();
            var signupRequestSender = new SignupRequestSender();
            var sendMessageRequestSender = new SendMessageRequestSender();

            //Interactors
            var globalChatInteractor = new UserScreenInteractor(globalChatScreen,
                sendMessageRequestSender,
                (IReciever<dynamic>)globalChatMessageReciever,
                communicator);

            var loginInteractor = new UserScreenInteractor(loginScreen,
                loginRequestSender,
                (IReciever<dynamic>)usernameReciever,
                communicator);
            var signupInteractor = new UserScreenInteractor(signupScreen,
                signupRequestSender,
                (IReciever<dynamic>)usernameReciever,
                communicator);

            //Interactor dictionaries
            var userActionToInteractor = new Dictionary<UserAction, IInteractor>
            {
                { UserAction.Login, loginInteractor },
                { UserAction.Signup, signupInteractor},
            };
            var chatScreenToInteractor = new Dictionary<IScreen, IInteractor>
            {
                { globalChatScreen, globalChatInteractor}
            };

            //DisplayRunners
            var chatDisplayRunner = new ChatDisplayRunner(chatScreenToInteractor);
            var startupDisplayRunner = new StartupDisplayRunner(welcomeScreen,
                userActionToInteractor,
                userActionReciever);
            return new Client(startupDisplayRunner, chatDisplayRunner, communicatorProcessor, communicator);
        }
    }
}
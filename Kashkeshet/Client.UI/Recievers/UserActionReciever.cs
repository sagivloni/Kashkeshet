﻿using Client.UI.Types;
using Common.IO.Readers.Abstractions;
using Common.IO.Recievers.Abstractions;
using Common.IO.Writers.Abstractions;
using System;
using System.Configuration;

namespace Client.UI.Recievers
{
    public class UserActionReciever : IReciever<UserAction>
    {
        private IReader _reader;
        private IWriter _writer;
        private string _userActionRequestMessage = ConfigurationManager.AppSettings["userActionRequestMessage"];

        public UserActionReciever(IReader reader, IWriter writer)
        {
            _reader = reader;
            _writer = writer;
        }

        public UserAction Recieve()
        {
            string input;
            int userAction;
            bool isValid;
            do
            {
                _writer.Write(_userActionRequestMessage);
                input = _reader.Read();
                isValid = int.TryParse(input, out userAction);
            } while (!isValid || Enum.IsDefined(typeof(UserAction), userAction));
            return (UserAction)userAction;
        }
    }
}
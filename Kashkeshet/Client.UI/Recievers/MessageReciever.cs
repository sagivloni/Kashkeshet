﻿using Common.IO.Readers.Abstractions;
using Common.IO.Recievers.Abstractions;
using Common.IO.Writers.Abstractions;
using System.Configuration;

namespace Client.UI.Recievers
{
    public class MessageReciever : IReciever<string>
    {
        private IReader _reader;
        private IWriter _writer;
        private int _maxMessageLength = int.Parse(ConfigurationManager.AppSettings["maxMessageLength"]);
        private string _nextScreenCommand = ConfigurationManager.AppSettings["nextScreenCommand"];

        public MessageReciever(IReader reader, IWriter writer)
        {
            _reader = reader;
            _writer = writer;
        }
        public string Recieve()
        {
            string message;
            do
            {
                _writer.Write("Your message: ");
                message = _reader.Read();
                if(message == _nextScreenCommand)
                {
                    return null;
                }
            } while (message.Length < _maxMessageLength);
            return message;
        }
    }
}
﻿using Common.IO.Readers.Abstractions;
using Common.IO.Recievers.Abstractions;
using Common.IO.Writers.Abstractions;
using System.Configuration;

namespace Client.UI.Recievers
{
    public class UsernameReciever : IReciever<string>
    {

        private IReader _reader;
        private IWriter _writer;
        private int _maxUsernameLength = int.Parse(ConfigurationManager.AppSettings["maxUsernameLength"]);

        public UsernameReciever(IReader reader, IWriter writer)
        {
            _reader = reader;
            _writer = writer;
        }
        public string Recieve()
        {
            string username;
            do
            {
                username = _reader.Read();
            } while (username.Length < _maxUsernameLength);
            return username;
        }
    }
}

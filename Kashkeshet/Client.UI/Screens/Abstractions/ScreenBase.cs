﻿using Common.IO.Readers.Abstractions;
using Common.IO.Writers.Abstractions;
using System;

namespace Client.UI.Screens.Abstractions
{
    public abstract class ScreenBase : IScreen
    {
        protected IWriter Writer;

        public ScreenBase(IWriter writer) => Writer = writer;

        public abstract void Print();
    }
}

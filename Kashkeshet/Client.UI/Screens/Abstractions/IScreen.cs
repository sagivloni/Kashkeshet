﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.UI.Screens.Abstractions
{
    public interface IScreen
    {
        void Print();
    }
}

﻿using Client.UI.Screens.Abstractions;
using Common.IO.Writers.Abstractions;
using System;
using System.Configuration;

namespace Client.UI.Screens
{
    public class WelcomeScreen : ScreenBase
    {
        private string _welcomeMessage = ConfigurationManager.AppSettings["welcomeMessage"];
        
        public WelcomeScreen(IWriter writer): base(writer)
        {
        }
        
        public override void Print()
        {
            Writer.Write($"{_welcomeMessage}{Environment.NewLine}");
        }
    }
}

﻿using Client.UI.Screens.Abstractions;
using Common.IO.Writers.Abstractions;
using System;
using System.Configuration;

namespace Client.UI.Screens
{
    public class SignupScreen : ScreenBase
    {
        private string _signupMessage = ConfigurationManager.AppSettings["signupMessage"];
        
        public SignupScreen(IWriter writer): base(writer)
        {
        }
        
        public override void Print()
        {
            Writer.Write($"{_signupMessage}{Environment.NewLine}");
        }
    }
}

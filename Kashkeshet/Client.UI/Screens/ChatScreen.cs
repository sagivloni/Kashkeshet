﻿using Client.UI.Screens.Abstractions;
using Common.IO.Writers.Abstractions;
using Common.Types.DTOs;
using System;

namespace Client.UI.Screens
{
    public class ChatScreen : ScreenBase
    {
        private ChatInfo _chat;

        public ChatScreen(IWriter writer, ChatInfo chat): base(writer)
        {
            _chat = chat;
        }
        
        public override void Print()
        {
            var chatString = $"Chat: {_chat.Name}{Environment.NewLine}"
                + $"Members: {GetChatMembers()}{Environment.NewLine}"
                + $"Connected members: {GetConnectedChatMembers()}{Environment.NewLine}{Environment.NewLine}"
                + GetChatHistory();
            Writer.Write(chatString);
        }

        private string GetChatMembers()
        {
            var memberString = "";
            foreach (var member in _chat.Members)
            {
                memberString += member.Name + " ";
            }
            return memberString;
        }

        private string GetConnectedChatMembers()
        {
            var memberString = "";
            foreach (var member in _chat.Members)
            {
                if(member.Status == UserStatus.Connected)
                {
                    memberString += member.Name + " ";
                }
            }
            return memberString;
        }

        private string GetChatHistory()
        {
            var historyString = "";
            foreach (var message in _chat.History)
            {
                historyString += $"{message.CreationTime}:{message.Author}{Environment.NewLine}"
                    + $"{message.Content}{Environment.NewLine}";
            }
            return historyString;
        }
    }
}

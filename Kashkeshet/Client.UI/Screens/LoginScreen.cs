﻿using Client.UI.Screens.Abstractions;
using Common.IO.Writers.Abstractions;
using System;
using System.Configuration;

namespace Client.UI.Screens
{
    public class LoginScreen : ScreenBase
    {
        private string _loginMessage = ConfigurationManager.AppSettings["loginMessage"];
        
        public LoginScreen(IWriter writer): base(writer)
        {
        }
        
        public override void Print()
        {
            Writer.Write($"{_loginMessage}{Environment.NewLine}");
        }
    }
}

﻿using Client.BL.RequestSenders.Abstraction;
using Client.UI.Interactors.Abstractions;
using Client.UI.Screens.Abstractions;
using Common.IO.Communicators.Abstractions;
using Common.IO.Recievers.Abstractions;

namespace Client.UI.Interactors
{
    public class UserScreenInteractor : IInteractor
    {
        private IScreen _screen;
        private IClientRequestSender _requestSender;
        private ICommunicator _communicator;
        private IReciever<dynamic> _reciever;

        public UserScreenInteractor(IScreen screen,
            IClientRequestSender requestSender,
            IReciever<dynamic> reciever,
            ICommunicator communicator)
        {
            _reciever = reciever;
            _screen = screen;
            _communicator = communicator;
            _requestSender = requestSender;
        }

        public bool Interact()
        {
            _screen.Print();
            var request = _reciever.Recieve();
            if(request == null)
            {
                return false;
            }
            _requestSender.SendRequest(request, _communicator);
            return true;
        }
    }
}

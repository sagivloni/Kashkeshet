﻿namespace Client.UI.Interactors.Abstractions
{
    public interface IInteractor
    {
        bool Interact();
    }
}

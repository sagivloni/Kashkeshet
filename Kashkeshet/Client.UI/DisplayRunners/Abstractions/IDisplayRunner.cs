﻿namespace Client.UI.DisplayRunners.Abstractions
{
    public interface IDisplayRunner
    {
        void Run();
    }
}

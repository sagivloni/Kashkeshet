﻿using Client.UI.DisplayRunners.Abstractions;
using Client.UI.Interactors.Abstractions;
using Client.UI.Screens.Abstractions;
using System.Collections.Generic;

namespace Client.UI.DisplayRunners
{
    public class ChatDisplayRunner : IDisplayRunner
    {
        private LinkedListNode<IScreen> _currentScreen;
        private LinkedList<IScreen> _chatScreens;
        private IDictionary<IScreen, IInteractor> _chatScreenToInteractor;

        public ChatDisplayRunner(IDictionary<IScreen, IInteractor> chatScreenToInteractor)
        {
            _chatScreenToInteractor = chatScreenToInteractor;
            _chatScreens = new LinkedList<IScreen>(_chatScreenToInteractor.Keys);
            _currentScreen = _chatScreens.First;
        }

        public void Run()
        {
            while (true)
            {
                var screen = _currentScreen.Value;
                if (!_chatScreenToInteractor[screen].Interact())
                {
                    _currentScreen = _currentScreen.Next ?? _chatScreens.First;
                }
            }
        }
    }
}

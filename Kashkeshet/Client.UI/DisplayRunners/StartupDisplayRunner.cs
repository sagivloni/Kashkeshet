﻿using Client.UI.DisplayRunners.Abstractions;
using Client.UI.Interactors.Abstractions;
using Client.UI.Screens.Abstractions;
using Client.UI.Types;
using Common.IO.Recievers.Abstractions;
using System.Collections.Generic;

namespace Client.UI.DisplayRunners
{
    public class StartupDisplayRunner : IDisplayRunner
    {
        private IScreen _welcomeScreen;
        private IDictionary<UserAction, IInteractor> _userActionToInteractor;
        private IReciever<UserAction> _actionReciever;
        
        public StartupDisplayRunner(IScreen welcomeScreen,
            IDictionary<UserAction, IInteractor> userActionToInteractor,
            IReciever<UserAction> actionReciever)
        {
            _welcomeScreen = welcomeScreen;
            _userActionToInteractor = userActionToInteractor;
            _actionReciever = actionReciever;
        }

        public void Run()
        {
            _welcomeScreen.Print();
            var userAction = _actionReciever.Recieve();
            _userActionToInteractor[userAction].Interact();
        }
    }
}
